package com.sq.beatles.test;

import com.sq.beatles.pojo.Product;
import com.sq.beatles.pojo.Category;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class JunitTest {

    ApplicationContext context = null;

    @Before
    public void factory() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }


    @Test
    public void getbeans() {
        Product p = (Product) context.getBean("product");
        p.setId(1);
        p.setName(" Product");
        Category category = new Category();
        category.setName("Category");
        category.setId(1);
        System.out.println(p);
        System.out.println(context);

    }

    @Test
    public void getPojo() {
        System.out.println(1);
    }


}
