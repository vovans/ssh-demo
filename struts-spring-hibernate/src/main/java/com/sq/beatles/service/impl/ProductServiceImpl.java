package com.sq.beatles.service.impl;

import com.sq.beatles.dao.ProductDao;
import com.sq.beatles.pojo.Product;
import com.sq.beatles.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDao productDao;


    @Override
    public List getProduct(int id) {
        List list = productDao.getProduct(id);
        return list;

    }

    @Override
    public Product delProduct(int id) {
        Product product = productDao.delProduct(id);
        return product;
    }
}
