package com.sq.beatles.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("category")
public class Category {
    private int id;
    private String name;
}
