package com.sq.beatles.dao;

import com.sq.beatles.pojo.Product;

import java.util.List;

public interface ProductDao {
    public List getProduct(int id);

    public Product delProduct(int id);
}
