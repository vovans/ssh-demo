package com.sq.beatles.dao;

import com.sq.beatles.pojo.Color;

import java.util.List;

public interface ColorDao {
    public List getColor(int id);

    public Color delProduct(int id);
}
