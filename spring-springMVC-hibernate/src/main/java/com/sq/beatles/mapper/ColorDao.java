package com.sq.beatles.mapper;


import com.sq.beatles.pojo.Color;

import java.util.List;

public interface ColorDao {
    public List getColor(int id);

    public Color delProduct(int id);
}
