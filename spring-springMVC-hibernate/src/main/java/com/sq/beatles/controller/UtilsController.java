package com.sq.beatles.controller;

import com.alibaba.fastjson.JSON;
import com.sq.beatles.pojo.Product;
import com.sq.beatles.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class UtilsController {
    @Autowired
    ProductService productService;

    @RequestMapping("/showProduct")
    public ModelAndView showProduct() {
        ModelAndView mav = new ModelAndView("product");
        List products = productService.getProduct(1);
        Product product = (Product) products.get(0);
        System.out.println(product);
        mav.addObject("product", product);
        String s = JSON.toJSONString(product);
        System.out.println(s);
        return mav;
    }

    @RequestMapping("/productList")
    public ModelAndView show() {
        //redirect:index.jsp
        ModelAndView mav = new ModelAndView("productList");
        List product = productService.getProduct(1);
        mav.addObject("product", product);
        String s = JSON.toJSONString(product);
        return mav;
    }
}
