package com.sq.beatles.service;

import com.sq.beatles.pojo.Product;

import java.util.List;

public interface ProductService {
    public List getProduct(int id);

    public Product delProduct(int id);
}
