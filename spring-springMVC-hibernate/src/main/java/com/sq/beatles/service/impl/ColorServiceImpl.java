package com.sq.beatles.service.impl;

import com.sq.beatles.pojo.Color;
import com.sq.beatles.service.ColorService;
import com.sq.beatles.mapper.ColorDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaochen
 */
@Service
public class ColorServiceImpl implements ColorService {
    @Autowired
    private ColorDao colorDao;


    @Override
    public List getColor(int id) {
        return colorDao.getColor(id);
    }

    @Override
    public Color delProduct(int id) {
        return null;
    }
}
